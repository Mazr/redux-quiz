import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Detials.less'
import {NavLink} from "react-router-dom";
import {bindActionCreators} from "redux";
import {deleteNote} from "../action/noteAction";

const mapStateToProps = state => ({
  noteList: state.notes
});
const mapDispatchToProps = (dispatch) => bindActionCreators({
  deleteNote
}, dispatch);

class Detials extends Component {

  constructor(props, context) {
    super(props, context);
  }
  render() {
    const notes = this.props.noteList;
    const note = notes.find(note => note.id == this.props.match.params.id);

    return (
      <div className={'detail'}>
        <aside className={'detail-aside'}>
          <ul>
            {
              notes.map(note => (
                <li key={note.id}><NavLink to={'/description/' + note.id}>{note.title}</NavLink></li>
              ))
            }
          </ul>
        </aside>
        <article className={'detail-article'}>
          {(note === undefined) ? <p>loading...</p> : (
            <>
              <h2>{note.title}</h2>
              <hr/>
              <p>{note.description}</p>
              <hr/>
              <NavLink to={'/'}>
              <button onClick={() => (this.props.deleteNote(note.id))}>
                删除
              </button>
              </NavLink>
              <NavLink to={'/'}>
              <button>
                返回
              </button>
              </NavLink>
            </>
          )}
        </article>
      </div>
    );
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detials);