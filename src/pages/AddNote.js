import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import {createNote, getDescriptionValue, getTitleValue} from "../action/noteAction";
import {NavLink} from "react-router-dom";
import Markdown from "react-textarea-markdown";


const mapDispatchToProps = dispatch => (
  bindActionCreators({
    createNote,
    getDescriptionValue,
    getTitleValue
  },dispatch)
);

const mapStateToProps = state => ({
  createData: state.createData
});


class AddNote extends Component {
  render() {

    return (
      <div>
        <form>
          <h1>创建笔记</h1>
          <hr/>

          <label>
            <p>标题</p>
          <input onChange={this.props.getTitleValue}/>
          </label>
          <label>
            <p>正文</p>
            <textarea onChange={this.props.getDescriptionValue}/>
          </label>
          <NavLink to={'/'}><button onClick={() => this.props.createNote(this.props.createData)}>创建</button></NavLink>
          <NavLink to={'/'}><button>取消</button></NavLink>
        </form>
      </div>
    );
  }

}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AddNote);