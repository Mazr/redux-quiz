import React, {Component} from 'react';
import './App.less';
import {Route, Switch} from "react-router";
import {BrowserRouter as Router} from "react-router-dom";
import Home from './home'
import Detials from "./pages/Detials";
import {bindActionCreators} from "redux";
import {getNoteList} from "./action/noteAction";
import {connect} from "react-redux";
import AddNote from "./pages/AddNote";

class App extends Component {

  constructor(props, context) {
    super(props, context);
  }

  componentDidMount() {
    {
      this.props.getNoteList()
    }
  }

  render() {
    return (
      <div className='App'>
        <h1>NOTES</h1>
        <Router>
          <Switch>
            <Route exact path="/" component={Home}/>
            <Route path={'/description/:id'} component={Detials}/>
            <Route path={'/notes/create'} component={AddNote}/>
          </Switch>
        </Router>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => bindActionCreators({
  getNoteList
}, dispatch);


export default connect(null,mapDispatchToProps)(App);