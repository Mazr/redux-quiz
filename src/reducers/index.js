import {combineReducers} from "redux";
import NoteReducer from "./NoteReducer";
import CreateNoteReducer from "./CreateNoteReducer";

const reducers = combineReducers({
  notes: NoteReducer,
  createData: CreateNoteReducer
});
export default reducers;