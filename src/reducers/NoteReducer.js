
const initState = [];

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_NOTE_LIST':
      return ([
        ...action.payload
        ]
      );

    default:
      return state
  }
};
