const initState = {};

export default (state = initState, action) => {
  switch (action.type) {
    case 'GET_INPUT_DATA':
      return ({
          ...state,
          ...action.payload
        }
      );

    default:
      return state
  }
};