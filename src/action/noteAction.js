const getNoteList = () => (dispatch) => {
  fetchData().then(result => {
    dispatch({
      type: 'GET_NOTE_LIST',
      payload: result
    });
  })
};

const deleteNote = (id) => (dispatch) => {
  deleteData(id).then(() => getNoteList()(dispatch))
};

const createNote = (data) => (dispatch) => {
  createData(data).then(() => getNoteList()(dispatch))
};

const getTitleValue= (e) => (dispatch) => {
    dispatch({
      type:'GET_INPUT_DATA',
      payload: {
        title: e.target.value
    }
  })
};

const getDescriptionValue= (e) => (dispatch) => {
  dispatch({
    type:'GET_INPUT_DATA',
    payload: {
      description: e.target.value
    }
  })
};



async function fetchData() {
  try {
    const result = await fetch('http://localhost:8080/api/posts');
    return await result.json();
  } catch (e) {
    console.error(e);
  }
}

async function deleteData(id) {
  try {
    const response = await fetch(`http://localhost:8080/api/posts/${id}`, {
      method: 'DELETE'
    });
  } catch (e) {
    console.error(e);
  }
}

async function createData(data) {
  try {
    const response = await fetch(`http://localhost:8080/api/posts`, {
      method: 'POST',
      mode: 'cors',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    });
  } catch (e) {
    console.error(e);
  }
}


export {getNoteList, deleteNote,createNote,getTitleValue,getDescriptionValue};