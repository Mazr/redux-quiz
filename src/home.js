import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Link} from "react-router-dom";
import {MdNoteAdd} from 'react-icons/md';


class Home extends Component {

  constructor(props, context) {
    super(props, context);
  }

  render() {
    const noteList = this.props.noteList;
    return (
      <nav>
        <ul>
          {
            noteList.map(note => (
              <li key={note.id}>
                <Link to={'/description/' + note.id}>{note.title}</Link>
              </li>
            ))
          }
          <li><Link to={'/notes/create'}><MdNoteAdd/></Link></li>
        </ul>
      </nav>
    );
  }
}

const mapStateToProps = state => ({
  noteList: state.notes
});

export default connect(
  mapStateToProps,
  null
)(Home);